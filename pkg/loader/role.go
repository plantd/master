package loader

import (
	"fmt"
	"sync"

	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"

	"github.com/graph-gophers/dataloader"
	"golang.org/x/net/context"
)

type roleLoader struct {
}

func newRoleLoader() dataloader.BatchFunc {
	return roleLoader{}.loadBatch
}

func (ldr roleLoader) loadBatch(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
	var (
		n       = len(keys)
		results = make([]*dataloader.Result, n)
		wg      sync.WaitGroup
	)

	wg.Add(n)

	for i, key := range keys {
		go func(i int, key dataloader.Key) {
			defer wg.Done()
			role, err := ctx.Value("roleService").(*service.RoleService).FindByName(key.String())
			results[i] = &dataloader.Result{Data: role, Error: err}
		}(i, key)
	}

	wg.Wait()

	return results
}

func LoadRole(ctx context.Context, key string) (*model.Role, error) {
	var role *model.Role

	ldr, err := extract(ctx, roleLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(key))()
	if err != nil {
		return nil, err
	}

	role, ok := data.(*model.Role)
	if !ok {
		return nil, fmt.Errorf("wrong type: the expected type is %T but got %T", role, data)
	}

	return role, nil
}
