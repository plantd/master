package loader

import (
	"fmt"
	"sync"

	"gitlab.com/plantd/master/pkg/model"
	//"gitlab.com/plantd/master/pkg/service"

	"github.com/graph-gophers/dataloader"
	"golang.org/x/net/context"
)

func LoadModule(ctx context.Context, name string) (*model.Module, error) {
	ldr, err := extract(ctx, moduleLoaderKey)
	if err != nil {
		return &model.Module{}, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(name))()
	if err != nil {
		return &model.Module{}, err
	}

	module, ok := data.(*model.Module)
	if !ok {
		return &model.Module{}, fmt.Errorf("wrong type: the expected type is %T but got %T", module, data)
	}

	return module, nil
}

func LoadModules(ctx context.Context, names []string) (ModuleResults, error) {
	var results []ModuleResult

	ldr, err := extract(ctx, moduleLoaderKey)
	if err != nil {
		return results, err
	}

	data, errs := ldr.LoadMany(ctx, dataloader.NewKeysFromStrings(names))()

	for i, d := range data {
		var e error
		if errs != nil {
			e = errs[i]
		}

		module, ok := d.(*model.Module)
		if !ok && e == nil {
			e = fmt.Errorf("wrong type: expected %T got %T", module, d)
		}

		results = append(results, ModuleResult{Module: module, Error: e})
	}

	return results, nil
}

type ModuleResult struct {
	Module *model.Module
	Error  error
}

type ModuleResults []ModuleResult

func (results ModuleResults) WithoutErrors() []*model.Module {
	modules := make([]*model.Module, 0, len(results))
	for _, r := range results {
		if r.Error != nil {
			continue
		}
		modules = append(modules, r.Module)
	}
	return modules
}

type moduleGetter interface {
	Module(ctx context.Context, name string) (*model.Module, error)
}

type ModuleLoader struct {
	get moduleGetter
}

func newModuleLoader(client moduleGetter) dataloader.BatchFunc {
	return ModuleLoader{get: client}.loadBatch
}

func (ldr ModuleLoader) loadBatch(ctx context.Context, names dataloader.Keys) []*dataloader.Result {
	var (
		n       = len(names)
		results = make([]*dataloader.Result, n)
		wg      sync.WaitGroup
	)

	wg.Add(n)

	for i, name := range names {
		go func(i int, name dataloader.Key) {
			defer wg.Done()

			/*
			 *properties, err := ctx.Value("brokerService").(*service.BrokerService).GetModuleProperties(key.String())
			 *module := *model.Module{
			 *    Properties: properties,
			 *}
			 */
			data, err := ldr.get.Module(ctx, name.String())
			results[i] = &dataloader.Result{Data: data, Error: err}
		}(i, name)
	}

	wg.Wait()

	return results
}
