package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"
)

type configurationsConnectionResolver struct {
	configurations []*model.Configuration
	totalCount     int
	from           *string
	to             *string
}

func (r *configurationsConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *configurationsConnectionResolver) Edges() *[]*configurationsEdgeResolver {
	l := make([]*configurationsEdgeResolver, len(r.configurations))
	for i := range l {
		l[i] = &configurationsEdgeResolver{
			cursor: service.EncodeCursor(&(r.configurations[i].ID)),
			model:  r.configurations[i],
		}
	}
	return &l
}

func (r *configurationsConnectionResolver) PageInfo() *pageInfoResolver {
	return &pageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}
