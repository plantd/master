package resolver

import (
	"fmt"

	"gitlab.com/plantd/master/pkg/loader"
	"gitlab.com/plantd/master/pkg/model"

	"golang.org/x/net/context"
)

type moduleResolver struct {
	module *model.Module
}

type NewModuleArgs struct {
	Module *model.Module
	Name   string
}

func NewModule(ctx context.Context, args NewModuleArgs) (*moduleResolver, error) {
	var module *model.Module
	var err error

	switch {
	//case args.Module.ModuleName != "":
	//module = args.Module
	case args.Name != "":
		module, err = loader.LoadModule(ctx, args.Name)
	default:
		err = fmt.Errorf("Unable to resolve module")
	}

	if err != nil {
		return nil, err
	}

	return &moduleResolver{module: module}, nil
}

func (r *moduleResolver) ModuleName() string {
	return r.module.ModuleName
}

func (r *moduleResolver) ServiceName() string {
	return r.module.ServiceName
}

func (r *moduleResolver) State() *string {
	return &r.module.State
}

func (r *moduleResolver) Properties(args struct{ Filter *[]*string }) *[]*propertyResolver {
	if args.Filter == nil {
		// Return all the properties
		l := make([]*propertyResolver, len(r.module.Properties))
		for i := range l {
			l[i] = &propertyResolver{
				property: r.module.Properties[i],
			}
		}
		return &l
	} else {
		// Return only the filtered properties
		// Create a map of the filters
		m := make(map[string]bool)
		filters := *args.Filter
		for i := range filters {
			m[*filters[i]] = true
			//log.Print("Filtering: ", *filters[i])
		}
		// Iterate through the entire properties array
		var l []*propertyResolver
		for i := range r.module.Properties {
			if m[r.module.Properties[i].Key] {
				l = append(l, &propertyResolver{property: r.module.Properties[i]})
			}
		}
		return &l
	}
}

func (r *moduleResolver) Configuration() *configurationResolver {
	return &configurationResolver{
		conf: r.module.Configuration,
	}
}

func (r *moduleResolver) Status() *statusResolver {
	return &statusResolver{
		status: r.module.Status,
	}
}

func (r *moduleResolver) Jobs() *[]*jobResolver {
	// Return all of the jobs
	l := make([]*jobResolver, len(r.module.Jobs))
	for i := range l {
		l[i] = &jobResolver{
			job: r.module.Jobs[i],
		}
	}
	return &l
}
