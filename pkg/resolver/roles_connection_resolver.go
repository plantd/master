package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"
)

type rolesConnectionResolver struct {
	roles      []*model.Role
	totalCount int
	from       *string
	to         *string
}

func (r *rolesConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *rolesConnectionResolver) Edges() *[]*rolesEdgeResolver {
	l := make([]*rolesEdgeResolver, len(r.roles))
	for i := range l {
		l[i] = &rolesEdgeResolver{
			cursor: service.EncodeCursor(&(r.roles[i].ID)),
			model:  r.roles[i],
		}
	}
	return &l
}

func (r *rolesConnectionResolver) PageInfo() *pageInfoResolver {
	return &pageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}
