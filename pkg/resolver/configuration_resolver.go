package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	graphql "github.com/graph-gophers/graphql-go"
)

type configurationResolver struct {
	conf *model.Configuration
}

func (r *configurationResolver) ID() graphql.ID {
	return graphql.ID(r.conf.ID)
}

func (r *configurationResolver) Name() *string {
	return &r.conf.Name
}

func (r *configurationResolver) Namespace() *string {
	return &r.conf.Namespace
}

func (r *configurationResolver) Objects() *[]*objectResolver {
	l := make([]*objectResolver, len(r.conf.Objects))

	for i := range l {
		l[i] = &objectResolver{
			object: r.conf.Objects[i],
		}
	}

	return &l
}

func (r *configurationResolver) Properties() *[]*propertyResolver {
	l := make([]*propertyResolver, len(r.conf.Properties))

	for i := range l {
		l[i] = &propertyResolver{
			property: r.conf.Properties[i],
		}
	}

	return &l
}
