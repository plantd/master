package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	graphql "github.com/graph-gophers/graphql-go"
)

type objectResolver struct {
	object *model.Object
}

func (r *objectResolver) ID() graphql.ID {
	return graphql.ID(r.object.ID)
}

func (r *objectResolver) Name() *string {
	return &r.object.Name
}

func (r *objectResolver) Type() *string {
	return &r.object.Type
}

func (r *objectResolver) Objects() *[]*objectResolver {
	l := make([]*objectResolver, len(r.object.Objects))

	for i := range l {
		l[i] = &objectResolver{
			object: r.object.Objects[i],
		}
	}

	return &l
}

func (r *objectResolver) Properties() *[]*propertyResolver {
	l := make([]*propertyResolver, len(r.object.Properties))

	for i := range l {
		l[i] = &propertyResolver{
			property: r.object.Properties[i],
		}
	}

	return &l
}
