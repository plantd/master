package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) CreateRole(ctx context.Context, args *struct {
	Name string
}) (*roleResolver, error) {
	role := &model.Role{
		Name: args.Name,
	}

	role, err := ctx.Value("roleService").(*service.RoleService).CreateRole(role)
	if err != nil {
		log.Errorf("Graphql error : %v", err)
		return nil, err
	}
	log.Debugf("Created role : %v", *role)
	return &roleResolver{role}, nil
}
