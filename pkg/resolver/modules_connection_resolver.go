package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"
)

type modulesConnectionResolver struct {
	modules    []*model.Module
	totalCount int
	from       *string
	to         *string
}

func (r *modulesConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *modulesConnectionResolver) Edges() *[]*modulesEdgeResolver {
	l := make([]*modulesEdgeResolver, len(r.modules))
	for i := range l {
		l[i] = &modulesEdgeResolver{
			cursor: r.modules[i].ModuleName,
			model:  r.modules[i],
		}
	}
	return &l
}

func (r *modulesConnectionResolver) PageInfo() *pageInfoResolver {
	return &pageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}
