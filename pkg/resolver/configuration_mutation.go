package resolver

import (
	"errors"
	"strings"

	"gitlab.com/plantd/master/pkg/service"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

type configurationInput struct {
	ID         *string
	Name       *string
	Namespace  string
	Objects    *[]*objectInput
	Properties *[]*propertyInput
}

type objectInput struct {
	ID         *string
	Name       *string
	Objects    *[]*objectInput
	Properties *[]*propertyInput
}

type propertyInput struct {
	Key   string
	Value string
}

// Converts an array of propertyInput to an array of protobuf properties
func ConvertProperties(input []*propertyInput) []*pb.Property {
	var properties []*pb.Property
	for _, property := range input {
		properties = append(properties, &pb.Property{
			Key:   property.Key,
			Value: property.Value,
		})
	}
	return properties
}

// ConvertObjects converts an array of objectInput to an array of protobuf objects
func ConvertObjects(input []*objectInput) []*pb.Object {
	if len(input) < 1 {
		return nil
	}
	var objects []*pb.Object
	for _, object := range input {
		writeObject := &pb.Object{}
		if object.ID != nil {
			writeObject.Id = *object.ID
		}
		if object.Name != nil {
			writeObject.Name = *object.Name
		}
		if object.Objects != nil {
			writeObject.Objects = ConvertObjects(*object.Objects)
		}
		if object.Properties != nil {
			writeObject.Properties = ConvertProperties(*object.Properties)
		}
		objects = append(objects, writeObject)
	}
	return objects
}

func (r *Resolver) CreateConfiguration(ctx context.Context, args struct {
	Conf configurationInput
}) (*configurationResolver, error) {

	// Check if the namespace is valid
	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(args.Conf.Namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Defaults for optional arguments
	var id string
	if args.Conf.ID != nil {
		id = *args.Conf.ID
	}
	var name string
	if args.Conf.Name != nil {
		name = *args.Conf.Name
	}
	var objects []*pb.Object
	if args.Conf.Objects != nil {
		objects = ConvertObjects(*args.Conf.Objects)
	}

	// Add the rest of the properties
	var properties []*pb.Property
	if args.Conf.Properties != nil {
		properties = ConvertProperties(*args.Conf.Properties)
	}

	// Add the author property
	userID := ctx.Value("user_id").(*string)
	user, err := ctx.Value("userService").(*service.UserService).FindByID(*userID)
	if err == nil && user.Email != "" {
		properties = append(properties, &pb.Property{
			Key:   "author",
			Value: user.Email,
		})
	}

	// Create the a protobuf configuration
	request := &pb.Configuration{
		Id:         id,
		Name:       name,
		Namespace:  pb.Configuration_Namespace(namespace),
		Objects:    objects,
		Properties: properties,
	}

	// Send the configuration to the configure service
	config, err := ctx.Value("confService").(*service.ConfigureService).CreateConfiguration(request)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &configurationResolver{config}, nil
}

func (r *Resolver) UpdateConfiguration(ctx context.Context, args struct {
	Id        string
	Namespace string
	Conf      configurationInput
}) (*configurationResolver, error) {
	// Check if the new namespace is valid
	targetNamespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(args.Conf.Namespace)]
	if !ok {
		return nil, errors.New("Target namespace provided is invalid")
	}

	// Defaults for optional arguments
	var id string
	if args.Conf.ID != nil {
		id = *args.Conf.ID
	}
	var name string
	if args.Conf.Name != nil {
		name = *args.Conf.Name
	}
	var objects []*pb.Object
	if args.Conf.Objects != nil {
		objects = ConvertObjects(*args.Conf.Objects)
	}
	var properties []*pb.Property
	if args.Conf.Properties != nil {
		properties = ConvertProperties(*args.Conf.Properties)
	}

	// Create the a protobuf configuration
	request := &pb.Configuration{
		Id:         id,
		Name:       name,
		Namespace:  pb.Configuration_Namespace(targetNamespace),
		Objects:    objects,
		Properties: properties,
	}

	// Send the configuration update request to the configure service
	config, err := ctx.Value("confService").(*service.ConfigureService).UpdateConfiguration(args.Id, args.Namespace, request)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &configurationResolver{config}, nil
}

func (r *Resolver) DeleteConfiguration(ctx context.Context, args struct {
	Id        string
	Namespace string
}) (*configurationResolver, error) {
	config, err := ctx.Value("confService").(*service.ConfigureService).DeleteConfiguration(args.Id, args.Namespace)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return &configurationResolver{config}, nil
}
