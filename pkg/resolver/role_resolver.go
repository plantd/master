package resolver

import (
	"time"

	"gitlab.com/plantd/master/pkg/model"

	graphql "github.com/graph-gophers/graphql-go"
)

type roleResolver struct {
	role *model.Role
}

func (r *roleResolver) ID() graphql.ID {
	return graphql.ID(r.role.ID)
}

func (r *roleResolver) Name() *string {
	return &r.role.Name
}

func (r *roleResolver) CreatedAt() (*graphql.Time, error) {
	if r.role.CreatedAt == "" {
		return nil, nil
	}

	t, err := time.Parse(time.RFC3339, r.role.CreatedAt)

	return &graphql.Time{Time: t}, err
}
