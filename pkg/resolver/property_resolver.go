package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
)

type propertyResolver struct {
	property *model.Property
}

func (r *propertyResolver) Key() *string {
	return &r.property.Key
}

func (r *propertyResolver) Value() *string {
	return &r.property.Value
}
