package resolver

import (
	"errors"
	"fmt"
	"time"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"

	"golang.org/x/net/context"
)

func (r *Resolver) Experiments(ctx context.Context, args struct {
	ServiceName string
}) (*configurationResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Use the broker service to submit the request
	service := ctx.Value("brokerService").(*service.BrokerService)
	configuration, err := service.GetModuleConfiguration(args.ServiceName, "experiment")
	if err != nil {
		fmt.Errorf("GetModuleConfiguration error: %v", err)
		return nil, err
	}
	return &configurationResolver{configuration}, nil
}

func (r *Resolver) Experiment(ctx context.Context, args struct {
	ID string
}) (*experimentResolver, error) {

	// Mock Module
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       model.ModuleState[model.ModuleStateConnected],
		Properties: []*model.Property{
			&model.Property{
				Key:   "type",
				Value: "camera",
			},
		},
	}
	// Mock Experiment
	someExperiment := &model.Experiment{
		ID:     args.ID,
		Status: model.ExperimentStatus[model.ExperimentStatusReady],
		Modules: []*model.Module{
			cameraModule,
		},
	}

	return &experimentResolver{someExperiment}, nil
}

func (r *Resolver) CurrentExperiment(ctx context.Context) (*experimentResolver, error) {
	// Mock Module
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       model.ModuleState[model.ModuleStateConnected],
		Properties: []*model.Property{
			&model.Property{
				Key:   "type",
				Value: "camera",
			},
		},
	}
	// Set some times for start, estimated duration, and time elapsed. Change these values to test.
	startTime := float64(1550182750324)
	estimatedDuration := float64(9850324)
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	timeElapsed := currentTime - startTime
	var status string
	if estimatedDuration <= timeElapsed {
		status = model.ExperimentStatus[model.ExperimentStatusStopped]
	} else {
		status = model.ExperimentStatus[model.ExperimentStatusRunning]
	}
	activeExperiment := &model.Experiment{
		ID:     "54321",
		Status: status,
		Modules: []*model.Module{
			cameraModule,
		},
		StartTime:         startTime,
		EstimatedDuration: estimatedDuration,
		TimeElapsed:       timeElapsed,
	}

	// Normally you would search for the active experiment if there is any
	// For now, we will just send a mock object back and ensure the status is "RUNNING"
	if activeExperiment.Status != model.ExperimentStatus[model.ExperimentStatusRunning] {
		return nil, nil
	}
	return &experimentResolver{activeExperiment}, nil
}
