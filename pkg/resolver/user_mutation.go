package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) CreateUser(ctx context.Context, args *struct {
	Email    string
	Password string
}) (*userResolver, error) {
	user := &model.User{
		Email:     args.Email,
		Password:  args.Password,
		IPAddress: *ctx.Value("requester_ip").(*string),
	}

	user, err := ctx.Value("userService").(*service.UserService).CreateUser(user)
	if err != nil {
		log.Errorf("Graphql error : %v", err)
		return nil, err
	}
	log.Debugf("Created user : %v", *user)
	return &userResolver{user}, nil
}
