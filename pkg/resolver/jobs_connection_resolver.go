package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"
)

type jobsConnectionResolver struct {
	jobs       []*model.Job
	totalCount int
	from       *string
	to         *string
}

func (r *jobsConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *jobsConnectionResolver) Edges() *[]*jobsEdgeResolver {
	l := make([]*jobsEdgeResolver, len(r.jobs))
	for i := range l {
		l[i] = &jobsEdgeResolver{
			cursor: service.EncodeCursor(&(r.jobs[i].ID)),
			model:  r.jobs[i],
		}
	}
	return &l
}

func (r *jobsConnectionResolver) PageInfo() *pageInfoResolver {
	return &pageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}
