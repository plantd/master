package resolver

import (
	"errors"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/loader"
	"gitlab.com/plantd/master/pkg/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) Role(ctx context.Context, args struct {
	Name string
}) (*roleResolver, error) {
	//Without using dataloader:
	//role, err := ctx.Value("roleService").(*service.RoleService).FindByName(args.Name)
	userId := ctx.Value("user_id").(*string)
	role, err := loader.LoadRole(ctx, args.Name)

	if err != nil {
		log.Errorf("Graphql error : %v", err)
		return nil, err
	}

	log.Debugf("Retrieved role by user_id[%s] : %v", *userId, *role)

	return &roleResolver{role}, nil
}

func (r *Resolver) Roles(ctx context.Context, args struct {
	First *int32
	After *string
}) (*rolesConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	userId := ctx.Value("user_id").(*string)

	roles, err := ctx.Value("roleService").(*service.RoleService).List(args.First, args.After)
	count, err := ctx.Value("roleService").(*service.RoleService).Count()
	log.Debugf("Retrieved roles by user_id[%s] :", *userId)
	config := ctx.Value("config").(*gcontext.Config)

	if config.Log.Debug {
		for _, role := range roles {
			log.Debugf("%v", *role)
		}
	}

	log.Debugf("Retrieved total roles count by user_id[%s] : %v", *userId, count)

	if err != nil {
		log.Errorf("Graphql error : %v", err)
		return nil, err
	}

	return &rolesConnectionResolver{
		roles:      roles,
		totalCount: count,
		from:       &(roles[0].ID),
		to:         &(roles[len(roles)-1].ID),
	}, nil
}
