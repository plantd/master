package resolver

import (
	"errors"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/loader"
	"gitlab.com/plantd/master/pkg/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) User(ctx context.Context, args struct {
	Email string
}) (*userResolver, error) {
	//Without using dataloader:
	//user, err := ctx.Value("userService").(*service.UserService).FindByEmail(args.Email)
	userId := ctx.Value("user_id").(*string)
	user, err := loader.LoadUser(ctx, args.Email)

	if err != nil {
		log.Errorf("GraphQL error : %v", err)
		return nil, err
	}

	log.Debugf("Retrieved user by user_id[%s] : %v", *userId, *user)

	return &userResolver{user}, nil
}

func (r *Resolver) Users(ctx context.Context, args struct {
	First *int32
	After *string
}) (*usersConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	userId := ctx.Value("user_id").(*string)

	users, err := ctx.Value("userService").(*service.UserService).List(args.First, args.After)
	count, err := ctx.Value("userService").(*service.UserService).Count()
	log.Debugf("Retrieved users by user_id[%s] :", *userId)
	config := ctx.Value("config").(*gcontext.Config)

	if config.Log.Debug {
		for _, user := range users {
			log.Debugf("%v", *user)
		}
	}

	log.Debugf("Retrieved total users count by user_id[%s] : %v", *userId, count)

	if err != nil {
		log.Errorf("GraphQL error : %v", err)
		return nil, err
	}

	return &usersConnectionResolver{
		users:      users,
		totalCount: count,
		from:       &(users[0].ID),
		to:         &(users[len(users)-1].ID),
	}, nil
}
