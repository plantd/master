package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	"github.com/graph-gophers/graphql-go"
)

type jobsEdgeResolver struct {
	cursor graphql.ID
	model  *model.Job
}

func (r *jobsEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *jobsEdgeResolver) Node() *jobResolver {
	return &jobResolver{job: r.model}
}
