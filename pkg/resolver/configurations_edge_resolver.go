package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	"github.com/graph-gophers/graphql-go"
)

type configurationsEdgeResolver struct {
	cursor graphql.ID
	model  *model.Configuration
}

func (r *configurationsEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *configurationsEdgeResolver) Node() *configurationResolver {
	return &configurationResolver{conf: r.model}
}
