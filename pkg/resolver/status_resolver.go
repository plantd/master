package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
)

type statusResolver struct {
	status *model.Status
}

func (r *statusResolver) Enabled() *bool {
	return &r.status.Enabled
}

func (r *statusResolver) Loaded() *bool {
	return &r.status.Loaded
}

func (r *statusResolver) Active() *bool {
	return &r.status.Active
}

func (r *statusResolver) Details() *[]*propertyResolver {
	l := make([]*propertyResolver, len(r.status.Details))
	for k, v := range r.status.Details {
		l = append(l, &propertyResolver{&model.Property{Key: k, Value: v}})
	}
	return &l
}
