package resolver

import (
	"errors"
	"fmt"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/service"

	"golang.org/x/net/context"
)

func (r *Resolver) Module(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
}) (*moduleResolver, error) {
	/*
	 *    broker := ctx.Value("brokerService").(*service.BrokerService)
	 *    moduleResponse, err := broker.GetModule(args.ServiceName, args.ModuleName)
	 *    if err != nil {
	 *        fmt.Errorf("GetModule error: %s", err)
	 *    }
	 *
	 *    return &moduleResolver{moduleResponse}, err
	 */

	return NewModule(ctx, NewModuleArgs{Name: args.ModuleName})
}

func (r *Resolver) Modules(ctx context.Context, args struct {
	First *int32
	After *string
}) (*modulesConnectionResolver, error) {
	// TODO: Allow serviceName argument instead of hardcoded 'acquire'
	broker := ctx.Value("brokerService").(*service.BrokerService)
	modulesResponse, err := broker.GetModules("acquire")
	if err != nil {
		fmt.Errorf("GetModules error: %s", err)
	}

	return &modulesConnectionResolver{
		modules:    *modulesResponse,
		totalCount: len(*modulesResponse),
		from:       &(*modulesResponse)[0].ModuleName,
		to:         &(*modulesResponse)[len(*modulesResponse)-1].ModuleName,
	}, nil

	// Mock Modules
	// cameraModule := &model.Module{
	// 	ModuleName:  "acquire-genicam",
	// 	ServiceName: "acquire",
	// 	State:       model.ModuleState[model.ModuleStateConnected],
	// 	Properties: []*model.Property{
	// 		&model.Property{
	// 			Key:   "type",
	// 			Value: "camera",
	// 		},
	// 	},
	// }
	// motorModule := &model.Module{
	// 	ModuleName:  "acquire-plc",
	// 	ServiceName: "acquire",
	// 	State:       model.ModuleState[model.ModuleStateConnected],
	// 	Properties: []*model.Property{
	// 		&model.Property{
	// 			Key:   "type",
	// 			Value: "motor",
	// 		},
	// 	},
	// }
	// modules := []*model.Module{
	// 	cameraModule,
	// 	motorModule,
	// }

	// return &modulesConnectionResolver{
	// 	modules:    modules,
	// 	totalCount: len(modules),
	// 	from:       &(modules[0].ModuleName),
	// 	to:         &(modules[len(modules)-1].ModuleName),
	// }, nil
}

func (r *Resolver) ModuleConfiguration(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
}) (*configurationResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	conf, err := broker.GetModuleConfiguration(args.ServiceName, args.ModuleName)
	if err != nil {
		fmt.Errorf("GetModuleConfiguration error: %s", err)
	}

	return &configurationResolver{conf}, err
}

func (r *Resolver) ModuleStatus(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
}) (*statusResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	stat, err := broker.GetModuleStatus(args.ServiceName, args.ModuleName)
	if err != nil {
		fmt.Errorf("GetModuleConfiguration error: %s", err)
	}

	return &statusResolver{stat}, err
}

func (r *Resolver) ModuleSettings(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
}) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	settings, err := broker.GetModuleSettings(args.ServiceName, args.ModuleName)
	if err != nil {
		fmt.Errorf("GetModuleSettings error: %s", err)
	}

	settingResolvers := make([]*propertyResolver, len(*settings))
	for index, Property := range *settings {
		settingResolvers[index] = &propertyResolver{Property}
	}

	return &settingResolvers, err
}

func (r *Resolver) ModuleJob(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	ID          string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.GetModuleJob(args.ServiceName, args.ModuleName, args.ID)
	if err != nil {
		fmt.Errorf("GetModuleJob error: %s", err)
	}

	return &jobResolver{job}, err
}

func (r *Resolver) ModuleJobs(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	First       *int32
	After       *string
}) (*jobsConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	jobs, err := broker.GetModuleJobs(args.ServiceName, args.ModuleName)
	if err != nil {
		fmt.Errorf("GetModuleJobs error: %s", err)
	}

	return &jobsConnectionResolver{
		jobs:       *jobs,
		totalCount: len(*jobs),
		from:       &(*jobs)[0].ID,
		to:         &(*jobs)[len(*jobs)-1].ID,
	}, nil

}

func (r *Resolver) ModuleProperty(ctx context.Context, args struct {
	ServiceName  string
	ModuleName   string
	PropertyName string
}) (*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	property, err := broker.GetModuleProperty(args.ServiceName, args.ModuleName, args.PropertyName)
	if err != nil {
		fmt.Errorf("GetModuleProperty error: %s", err)
		return nil, err
	}

	return &propertyResolver{property}, nil
}

func (r *Resolver) ModuleProperties(ctx context.Context, args struct {
	ServiceName   string
	ModuleName    string
	PropertyNames *[]*string
}) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Create an array of protobuf property from the entered names
	var reqProperties []*pb.Property
	if args.PropertyNames != nil {
		for i := range *args.PropertyNames {
			reqProperties = append(reqProperties, &pb.Property{
				Key: *(*args.PropertyNames)[i],
			})
		}
	}

	// Use the broker service
	broker := ctx.Value("brokerService").(*service.BrokerService)
	properties, err := broker.GetModuleProperties(args.ServiceName, args.ModuleName, reqProperties)
	if err != nil {
		fmt.Errorf("GetModuleProperties error: %s", err)
		return nil, err
	}

	// Create property resolvers for each property
	var _properties []*propertyResolver
	for _, property := range properties {
		_properties = append(_properties, &propertyResolver{property})
	}

	return &_properties, nil
}
