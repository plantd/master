package resolver

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/model"
	"gitlab.com/plantd/master/pkg/service"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) SubmitManagerCommand(ctx context.Context, args struct {
	Command string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	response := &model.Response{}
	var err error
	switch args.Command {
	case "start":
		response, err = ctx.Value("managerService").(*service.ManagerService).Start()
	case "stop":
		response, err = ctx.Value("managerService").(*service.ManagerService).Stop()
	case "restart":
		response, err = ctx.Value("managerService").(*service.ManagerService).Restart()
	case "status":
		err = fmt.Errorf("Invalid manager command. Use the managerStatus() GQL query instead")
	case "install":
		response, err = ctx.Value("managerService").(*service.ManagerService).Install()
	case "uninstall":
		response, err = ctx.Value("managerService").(*service.ManagerService).Uninstall()
	case "upgrade":
		response, err = ctx.Value("managerService").(*service.ManagerService).Upgrade()
	default:
		err = fmt.Errorf("Unknown manager command: %s", args.Command)
	}
	if err != nil {
		log.Print(err)
		return nil, err
	}

	// Convert the manager response into a job
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	job := &model.Job{
		ID:        uuid.New().String(),
		Status:    strconv.Itoa(response.Code),
		StartTime: currentTime,
		Properties: []*model.Property{
			&model.Property{
				Key:   "command",
				Value: args.Command,
			},
		},
	}

	return &jobResolver{job}, nil
}
