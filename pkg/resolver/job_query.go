package resolver

import (
	"time"

	"gitlab.com/plantd/master/pkg/model"

	"golang.org/x/net/context"
)

func (r *Resolver) Job(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	ID          string
}) (*jobResolver, error) {
	// MOCK MODULES
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       model.ModuleState[model.ModuleStateConnected],
		Properties: []*model.Property{
			&model.Property{
				Key:   "type",
				Value: "camera",
			},
			&model.Property{
				Key:   "FPS",
				Value: "2",
			},
			&model.Property{
				Key:   "button",
				Value: "true",
			},
		},
	}

	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	testJob := &model.Job{
		ID:        "1",
		Status:    model.JobStatus[model.JobStatusRunning],
		StartTime: currentTime,
		Module:    cameraModule,
		Properties: []*model.Property{
			&model.Property{
				Key:   "somekey",
				Value: "some value",
			},
		},
	}

	return &jobResolver{testJob}, nil
}

func (r *Resolver) Jobs(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	First       *int32
	After       *string
}) (*jobsConnectionResolver, error) {
	// MOCK MODULES
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       model.ModuleState[model.ModuleStateConnected],
		Properties: []*model.Property{
			&model.Property{
				Key:   "type",
				Value: "camera",
			},
		},
	}

	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	job1 := &model.Job{
		ID:        "1",
		Status:    model.JobStatus[model.JobStatusRunning],
		StartTime: currentTime,
		Module:    cameraModule,
		Properties: []*model.Property{
			&model.Property{
				Key:   "somekey",
				Value: "some value",
			},
		},
	}
	job2 := &model.Job{
		ID:     "2",
		Status: model.JobStatus[model.JobStatusSubmitted],
		Module: cameraModule,
	}

	jobs := []*model.Job{
		job1,
		job2,
	}

	return &jobsConnectionResolver{
		jobs:       jobs,
		totalCount: len(jobs),
		from:       &(jobs[0].ID),
		to:         &(jobs[len(jobs)-1].ID),
	}, nil
}
