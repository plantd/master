package resolver

import (
	"errors"
	"fmt"
	"strings"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/service"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) SubmitExpUpdateJob(ctx context.Context, args struct {
	ServiceName string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	jobProperties := []*pb.Property{{
		Key: "data-path", Value: "/srv/data/experiments",
	}}

	// Use the broker service to submit the request
	service := ctx.Value("brokerService").(*service.BrokerService)
	job, err := service.ModuleSubmitJob("state", "experiment", "update-experiments", "", jobProperties)
	if err != nil {
		fmt.Errorf("ModuleSubmitJob error: %v", err)
		return nil, err
	}

	return &jobResolver{job}, nil
}

func (r *Resolver) SubmitExpBundleJob(ctx context.Context, args struct {
	ServiceName   string
	ExperimentIds []*string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	var str strings.Builder
	for i := range args.ExperimentIds {
		// Append the experiment ID and comma delimit it
		str.WriteString(*args.ExperimentIds[i])
		str.WriteString(",")
	}

	var jobProperties []*pb.Property
	jobProperties = append(jobProperties, &pb.Property{
		Key:   "exp-id",
		Value: strings.TrimSuffix(str.String(), ","),
	})
	log.Print("Bundle experiment IDs: " + jobProperties[0].GetValue())

	// Use the broker service to submit the request
	service := ctx.Value("brokerService").(*service.BrokerService)
	job, err := service.ModuleSubmitJob(args.ServiceName, "experiment", "bundle-experiments", jobProperties[0].GetValue(), jobProperties)
	if err != nil {
		fmt.Errorf("ModuleSubmitJob error: %v", err)
		return nil, err
	}

	return &jobResolver{job}, nil
}

func (r *Resolver) SubmitExpDeleteJob(ctx context.Context, args struct {
	ServiceName   string
	ExperimentIds []*string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	var str strings.Builder
	for i := range args.ExperimentIds {
		// Append the experiment ID and comma delimit it
		str.WriteString(*args.ExperimentIds[i])
		str.WriteString(",")
	}

	var jobProperties []*pb.Property
	jobProperties = append(jobProperties, &pb.Property{
		Key:   "exp-id",
		Value: strings.TrimSuffix(str.String(), ","),
	})
	log.Print("Deleting experiment IDs: " + jobProperties[0].GetValue())

	// Use the broker service to submit the request
	service := ctx.Value("brokerService").(*service.BrokerService)
	job, err := service.ModuleSubmitJob(args.ServiceName, "experiment", "delete-experiments", jobProperties[0].GetValue(), jobProperties)
	if err != nil {
		fmt.Errorf("ModuleSubmitJob error: %v", err)
		return nil, err
	}

	return &jobResolver{job}, nil
}
