package resolver

import (
	"errors"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func (r *Resolver) Configuration(ctx context.Context, args struct {
	ID        string
	Namespace string
}) (*configurationResolver, error) {
	userId := ctx.Value("user_id").(*string)
	conf, err := ctx.Value("confService").(*service.ConfigureService).FindById(args.ID, args.Namespace)

	if err != nil {
		log.Debugf("GraphQL error : %v", err)
		return nil, err
	}

	log.Debugf("Retrieved configuration by user_id[%s] : %v", *userId, *conf)

	return &configurationResolver{conf}, nil
}

func (r *Resolver) Configurations(ctx context.Context, args struct {
	Namespace string
	First     *int32
	After     *string
}) (*configurationsConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	userId := ctx.Value("user_id").(*string)

	confs, err := ctx.Value("confService").(*service.ConfigureService).List(args.Namespace, args.First, args.After)
	count, err := ctx.Value("confService").(*service.ConfigureService).Count(args.Namespace)
	log.Debugf("Retrieved configurations by user_id[%s] :", *userId)
	config := ctx.Value("config").(*gcontext.Config)

	if config.Log.Debug {
		for _, conf := range confs {
			log.Debugf("%v", *conf)
		}
	}

	log.Debugf("Retrieved total configurations count by user_id[%s] : %v", *userId, count)

	if err != nil {
		log.Errorf("GraphQL error : %v", err)
		return nil, err
	}

	// Handle the case where there are no configurations
	var from *string
	var to *string
	if count > 0 {
		from = &(confs[0].ID)
		to = &(confs[len(confs)-1].ID)
	}

	return &configurationsConnectionResolver{
		configurations: confs,
		totalCount:     count,
		from:           from,
		to:             to,
	}, nil
}
