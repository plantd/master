package resolver

import (
	"gitlab.com/plantd/master/pkg/model"
)

type modulesEdgeResolver struct {
	cursor string
	model  *model.Module
}

func (r *modulesEdgeResolver) Cursor() string {
	return r.cursor
}

func (r *modulesEdgeResolver) Node() *moduleResolver {
	return &moduleResolver{module: r.model}
}
