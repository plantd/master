package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
)

type jobResolver struct {
	job *model.Job
}

func (r *jobResolver) ID() graphql.ID {
	return graphql.ID(r.job.ID)
}

func (r *jobResolver) StartTime() *float64 {
	startTime := &r.job.StartTime
	return startTime
}

func (r *jobResolver) FinishTime() *float64 {
	finishTime := &r.job.StartTime
	return finishTime
}

func (r *jobResolver) Status() *string {
	return &r.job.Status
}

func (r *jobResolver) Module() *moduleResolver {
	return &moduleResolver{r.job.Module}
}

func (r *jobResolver) Properties(args struct{ Filter *[]*string }) *[]*propertyResolver {
	if args.Filter == nil {
		// Return all the properties
		l := make([]*propertyResolver, len(r.job.Properties))
		for i := range l {
			l[i] = &propertyResolver{
				property: r.job.Properties[i],
			}
		}
		return &l
	} else {
		// Return only the filtered properties
		// Create a map of the filters
		m := make(map[string]bool)
		filters := *args.Filter
		for i := range filters {
			m[*filters[i]] = true
			log.Print(*filters[i])
		}
		// Iterate through the entire properties array
		var l []*propertyResolver
		for i := range r.job.Properties {
			if m[r.job.Properties[i].Key] {
				l = append(l, &propertyResolver{property: r.job.Properties[i]})
			}
		}
		return &l
	}
}
