package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	"github.com/graph-gophers/graphql-go"
)

type rolesEdgeResolver struct {
	cursor graphql.ID
	model  *model.Role
}

func (r *rolesEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *rolesEdgeResolver) Node() *roleResolver {
	return &roleResolver{role: r.model}
}
