package resolver

import (
	"errors"
	"fmt"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/service"

	"golang.org/x/net/context"
)

func (r *Resolver) ManagerStatus(ctx context.Context) (*[]*propertyResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Use the manager service
	response, err := ctx.Value("managerService").(*service.ManagerService).Status()
	if err != nil {
		fmt.Errorf("Manager error: %s", err)
		return nil, err
	}

	// Create an array of property resolvers
	var _properties []*propertyResolver
	for _, property := range *response {
		_properties = append(_properties, &propertyResolver{property})
	}

	return &_properties, nil
}
