package resolver

import (
	"errors"
	"fmt"

	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/service"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	"golang.org/x/net/context"
)

func (r *Resolver) SubmitJob(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	Action      string
	ActionValue *string
	Properties  *[]*propertyInput
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	// Parse the properties into a pb property array
	var jobProperties []*pb.Property
	if args.Properties != nil {
		jobProperties = ConvertProperties(*args.Properties)
	}

	// Parse the optional value
	var value string
	if args.ActionValue != nil {
		value = *args.ActionValue
	}

	// Use the broker service to submit the request
	service := ctx.Value("brokerService").(*service.BrokerService)
	job, err := service.ModuleSubmitJob(args.ServiceName, args.ModuleName, args.Action, value, jobProperties)
	if err != nil {
		fmt.Errorf("ModuleSubmitJob error: %v", err)
		return nil, err
	}

	return &jobResolver{job}, nil
}

func (r *Resolver) CancelJob(ctx context.Context, args struct {
	ServiceName string
	ModuleName  string
	JobId       string
}) (*jobResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	service := ctx.Value("brokerService").(*service.BrokerService)
	job, err := service.ModuleCancelJob(args.ServiceName, args.ModuleName, args.JobId)
	if err != nil {
		fmt.Errorf("ModuleCancelJob error: %s", err)
		return nil, err
	}

	return &jobResolver{job}, nil
}
