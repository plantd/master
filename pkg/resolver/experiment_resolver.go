package resolver

import (
	"gitlab.com/plantd/master/pkg/model"

	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
)

type experimentResolver struct {
	exp *model.Experiment
}

func (r *experimentResolver) ID() graphql.ID {
	return graphql.ID(r.exp.ID)
}

func (r *experimentResolver) Status() *string {
	status := &r.exp.Status
	if *status == "" {
		return nil
	}
	return status
}

func (r *experimentResolver) Name() *string {
	name := &r.exp.Name
	if *name == "" {
		return nil
	}
	return name
}

func (r *experimentResolver) StartTime() *float64 {
	startTime := &r.exp.StartTime
	return startTime
}

func (r *experimentResolver) EstimatedDuration() *float64 {
	estimatedDuration := &r.exp.EstimatedDuration
	return estimatedDuration
}

func (r *experimentResolver) TimeElapsed() *float64 {
	timeElapsed := &r.exp.TimeElapsed
	return timeElapsed
}

func (r *experimentResolver) Modules() *[]*moduleResolver {
	l := make([]*moduleResolver, len(r.exp.Modules))
	log.Println("hit")

	for i := range l {
		l[i] = &moduleResolver{
			module: r.exp.Modules[i],
		}
	}

	return &l
}

func (r *experimentResolver) Properties() *[]*propertyResolver {
	l := make([]*propertyResolver, len(r.exp.Properties))

	for i := range l {
		l[i] = &propertyResolver{
			property: r.exp.Properties[i],
		}
	}

	return &l
}
