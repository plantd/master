package service

import (
	//"os"

	"gitlab.com/plantd/master/pkg/context"

	"github.com/sirupsen/logrus"
)

type StandardLogger struct {
	*logrus.Logger
}

func NewLogger(config *context.Config) *StandardLogger {
	var baseLogger = logrus.New()
	var standardLogger = &StandardLogger{baseLogger}

	// TODO: figure out how to add default fields
	//standardLogger.Fields = logrus.Fields{"service": "broker"}

	// initialize logging
	switch config.Log.Formatter {
	case "json":
		standardLogger.Formatter = &logrus.JSONFormatter{}
	}

	// TODO: get output writer from config
	//standardLogger.Out = os.Stdout

	level, err := logrus.ParseLevel(config.Log.Level)
	if err == nil {
		standardLogger.Level = level
	}

	return standardLogger
}
