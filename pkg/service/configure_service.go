package service

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/plantd/master/pkg/model"

	pb "gitlab.com/plantd/broker/pkg/proto/v1"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

type ConfigureService struct {
	client pb.ConfigureEndpointClient
	log    *StandardLogger
}

func NewConfigureService(client pb.ConfigureEndpointClient, log *StandardLogger) *ConfigureService {
	return &ConfigureService{client, log}
}

func (c *ConfigureService) FindById(confId string, confNamespace string) (*model.Configuration, error) {
	configuration := &model.Configuration{}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Id:        confId,
		Namespace: pb.Configuration_Namespace(namespace),
	}
	response, err := c.client.Read(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not read configuration %s: %s", confId, err)
	}

	c.log.Debugf("gRPC conf: %s", response.Configuration.Id)

	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		return nil, err
	}

	c.log.Debugf("%v", configuration)

	return configuration, nil
}

func (c *ConfigureService) List(confNamespace string, first *int32, after *string) ([]*model.Configuration, error) {
	// TODO: Until go-plantd supports pagination for configurations, just load them all
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Namespace: pb.Configuration_Namespace(namespace),
	}

	response, err := c.client.List(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not read configurations: %s", err)
	}

	configurations := make([]*model.Configuration, len(response.Configurations))

	for i, element := range response.Configurations {
		configuration := &model.Configuration{}
		c.log.Debugf("Found and checking: %v", element)
		err = configuration.FromProtobuf(response.Configurations[i])
		if err != nil {
			return nil, err
		}
		configurations[i] = configuration
	}

	return configurations, nil
}

func (c *ConfigureService) Count(confNamespace string) (int, error) {
	// XXX: There might be a better way to do this. plantd-configure does not have a method to just get count.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return 0, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Namespace: pb.Configuration_Namespace(namespace),
	}

	response, err := c.client.List(ctx, request)
	if err != nil {
		return 0, fmt.Errorf("could not read configurations: %s", err)
	}

	return len(response.Configurations), nil
}

func (c *ConfigureService) CreateConfiguration(conf *pb.Configuration) (*model.Configuration, error) {
	log.Printf("Sending grpc request for CreateConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the GRPC request
	request := &pb.ConfigurationRequest{
		Id:            conf.Id,
		Namespace:     conf.Namespace,
		Configuration: conf,
	}

	// Send the request
	response, err := c.client.Create(ctx, request)
	if err != nil {
		c.log.Error("GRPC send issue")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		log.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}

func (c *ConfigureService) UpdateConfiguration(id string, namespace string, conf *pb.Configuration) (*model.Configuration, error) {
	log.Printf("Sending grpc request for UpdateConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Check if the namespace is valid
	_namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Create the GRPC request
	request := &pb.ConfigurationRequest{
		Id:            id,
		Namespace:     pb.Configuration_Namespace(_namespace),
		Configuration: conf,
	}

	// Send the request
	response, err := c.client.Update(ctx, request)
	if err != nil {
		c.log.Error("GRPC send issue for update")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		log.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}

func (c *ConfigureService) DeleteConfiguration(id string, namespace string) (*model.Configuration, error) {
	log.Printf("Sending grpc request for DeleteConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Check if the namespace is valid
	_namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Create the grpc request
	request := &pb.ConfigurationRequest{
		Id:        id,
		Namespace: pb.Configuration_Namespace(_namespace),
	}

	// Send the request
	response, err := c.client.Delete(ctx, request)
	log.Print(err)
	if err != nil {
		log.Print("error at response")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		log.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}
