package service

import (
	"fmt"
	"time"

	model "gitlab.com/plantd/master/pkg/model"
	pb "gitlab.com/plantd/plantctl/proto"

	"golang.org/x/net/context"
)

type ManagerService struct {
	client pb.ManagerClient
	log    *StandardLogger
}

func NewManagerService(client pb.ManagerClient, log *StandardLogger) *ManagerService {
	return &ManagerService{client, log}
}

// Start configured plantd services
func (c *ManagerService) Start() (*model.Response, error) {
	c.log.Debugf("Sending grpc Start request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Start(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Start to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Stop configured plantd services
func (c *ManagerService) Stop() (*model.Response, error) {
	c.log.Debugf("Sending grpc Stop request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Stop(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Stop to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Restart configured plantd services
func (c *ManagerService) Restart() (*model.Response, error) {
	c.log.Debugf("Sending grpc restart request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Restart(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Restart to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Check the status of configured plantd services
// rpc Status (Empty) returns (ServiceResponse) {}
func (c *ManagerService) Status() (*[]*model.Property, error) {
	c.log.Debugf("Sending grpc Status request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Send the status command via GRPC
	rawResponse, err := c.client.Status(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Status to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %d", rawResponse.Code)

	// Check if the response was successful
	if rawResponse.GetError() != nil {
		return nil, fmt.Errorf("Error response: %", rawResponse.GetError().String())
	}

	var properties []*model.Property
	for _, service := range rawResponse.GetServices() {
		properties = append(properties, &model.Property{
			Key:   service.GetTarget(),
			Value: service.GetState().String(),
		})
	}

	return &properties, nil
}

// TODO:
// // Stream log messages for configured plantd services
// rpc Watch (Empty) returns (stream LogEntry) {}

// // Receive updates when configured services change
// rpc Monitor (Empty) returns (stream Service) {}

// Install a plantd system
func (c *ManagerService) Install() (*model.Response, error) {
	c.log.Debugf("Sending grpc Install request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Install(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Install to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Uninstall a plantd system
func (c *ManagerService) Uninstall() (*model.Response, error) {
	c.log.Debugf("Sending grpc uninstall request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Uninstall(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Uninstall to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Upgrade a plantd system
func (c *ManagerService) Upgrade() (*model.Response, error) {
	c.log.Debugf("Sending grpc Upgrade request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Upgrade(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Upgrade to Manager: %s", err)
	}
	c.log.Debugf("gRPC response: %s", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}
