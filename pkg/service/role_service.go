package service

import (
	"database/sql"

	"gitlab.com/plantd/master/pkg/model"

	"github.com/jmoiron/sqlx"
	"github.com/rs/xid"
)

type RoleService struct {
	db  *sqlx.DB
	log *StandardLogger
}

func NewRoleService(db *sqlx.DB, log *StandardLogger) *RoleService {
	return &RoleService{db: db, log: log}
}

func (r *RoleService) FindByUserId(userId *string) ([]*model.Role, error) {
	roles := make([]*model.Role, 0)

	roleSQL := `SELECT role.*
	FROM roles role
	INNER JOIN rel_users_roles ur ON role.id = ur.role_id
	WHERE ur.user_id = $1`
	err := r.db.Select(&roles, roleSQL, userId)
	if err == sql.ErrNoRows {
		return roles, nil
	}
	if err != nil {
		return nil, err
	}
	return roles, nil
}

func (r *RoleService) FindByName(name string) (*model.Role, error) {
	role := &model.Role{}

	roleSQL := `SELECT * FROM roles WHERE name = $1;`
	rdb := r.db.Unsafe()
	row := rdb.QueryRowx(roleSQL, name)
	err := row.StructScan(role)
	if err == sql.ErrNoRows {
		return role, nil
	}
	if err != nil {
		r.log.Errorf("Error in retrieving role : %v", err)
		return nil, err
	}
	return role, nil
}

func (r *RoleService) CreateRole(role *model.Role) (*model.Role, error) {
	roleId := xid.New()
	role.ID = roleId.String()
	roleSQL := `INSERT
	INTO roles (id, name)
	VALUES(:id, :name)`
	_, err := r.db.NamedExec(roleSQL, role)
	if err != nil {
		return nil, err
	}
	return role, nil
}

func (r *RoleService) List(first *int32, after *string) ([]*model.Role, error) {
	roles := make([]*model.Role, 0)
	var fetchSize int32
	if first == nil {
		fetchSize = defaultListFetchSize
	} else {
		fetchSize = *first
	}

	if after != nil {
		roleSQL := `SELECT *
		FROM roles
		WHERE created_at < (SELECT created_at FROM roles WHERE id = $1)
		ORDER BY created_at DESC LIMIT $2;`
		decodedIndex, _ := DecodeCursor(after)
		err := r.db.Select(&roles, roleSQL, decodedIndex, fetchSize)
		if err != nil {
			return nil, err
		}
		return roles, nil
	}
	roleSQL := `SELECT * FROM roles ORDER BY created_at DESC LIMIT $1;`
	err := r.db.Select(&roles, roleSQL, fetchSize)
	if err != nil {
		return nil, err
	}
	return roles, nil
}

func (r *RoleService) Count() (int, error) {
	var count int
	roleSQL := `SELECT count(*) FROM roles`
	err := r.db.Get(&count, roleSQL)
	if err != nil {
		return 0, err
	}
	return count, nil
}
