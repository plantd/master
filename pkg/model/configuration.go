package model

import (
	pb "gitlab.com/plantd/broker/pkg/proto/v1"
)

type Configuration struct {
	ID         string
	Name       string
	Namespace  string
	Objects    []*Object
	Properties []*Property
}

func (c *Configuration) FromProtobuf(in *pb.Configuration) error {
	c.ID = in.Id
	c.Name = in.Name
	c.Namespace = in.Namespace.String()

	var objects []*Object
	for _, obj := range in.Objects {
		_obj := &Object{}
		err := _obj.FromProtobuf(obj)
		if err != nil {
			return err
		}
		objects = append(objects, _obj)
	}

	var properties []*Property
	for _, prop := range in.Properties {
		_prop := &Property{}
		err := _prop.FromProtobuf(prop)
		if err != nil {
			return err
		}
		properties = append(properties, _prop)
	}

	c.Objects = objects
	c.Properties = properties

	return nil
}
