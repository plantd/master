package model

type Event struct {
	ID          int32
	Name        string
	Description string
}
