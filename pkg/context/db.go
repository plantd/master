package context

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

func OpenDB(config *Config) (*sqlx.DB, error) {
	log.Printf("Connecting to database '%s' on %s:%s", config.DB.Name, config.DB.Host, config.DB.Port)
	db, err := sqlx.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			config.DB.Host, config.DB.Port, config.DB.User, config.DB.Password, config.DB.Name),
	)

	if err != nil {
		panic(err.Error())
	}

	if err = db.Ping(); err != nil {
		log.Fatalf("Unable to connect to db: %s \n", err)
		log.Println("Retry database connection in 5 seconds... ")
		time.Sleep(time.Duration(5) * time.Second)
		return OpenDB(config)
	}

	log.Println("Database is connected")

	return db, nil
}
