package context

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type database struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Name     string `mapstructure:"name"`
}

type auth struct {
	JWTSecret   string        `mapstructure:"jwt-secret"`
	JWTExpireIn time.Duration `mapstructure:"jwt-expire-in"`
}

type configure struct {
	Host   string `mapstructure:"host"`
	Port   string `mapstructure:"port"`
	UseTLS bool   `mapstructure:"tls"`
	CACert string `mapstructure:"cacert"`
	Cert   string `mapstructure:"cert"`
	Key    string `mapstructure:"key"`
}

type client struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

type logging struct {
	Debug     bool   `mapstructure:"debug"`
	Formatter string `mapstructure:"formatter"`
	Level     string `mapstructure:"level"`
}

type Config struct {
	App       string            `mapstructure:"app"`
	DB        database          `mapstructure:"db"`
	Auth      auth              `mapstructure:"auth"`
	Configure configure         `mapstructure:"configure"`
	Manager   client            `mapstructure:"manager"`
	Broker    map[string]client `mapstructure:"broker"`
	Log       logging           `mapstructure:"log"`
}

func LoadConfig(path string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	file := os.Getenv("PLANTD_MASTER_CONFIG")
	if file == "" {
		config.SetConfigName("master")
		config.AddConfigPath(".")
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
		config.AddConfigPath("/etc/plantd")
	} else {
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "hcl") ||
			strings.HasSuffix(base, "toml") ||
			strings.HasSuffix(base, "conf") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
		}
		config.SetConfigName(base)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_MASTER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	// initialize logging
	switch c.Log.Formatter {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	level, err := logrus.ParseLevel(c.Log.Level)
	if err == nil {
		logrus.SetLevel(level)
	}

	logrus.Debug("Configuration Location: ", config.ConfigFileUsed())

	return &c, nil
}
