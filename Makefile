PROJECT=master
REGISTRY=registry.gitlab.com/plantd/$(PROJECT)
REGISTRY_TAG=v2
PREFIX?=/usr
DESTDIR?=
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')
VERSION := $(shell git describe)
BINARY_NAME = "plantd-master"

all: build

print-version:
	@echo $(VERSION)

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: schema ; $(info $(M) Building project...)
	@env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o target/$(BINARY_NAME) -ldflags "-X main.VERSION=$(VERSION)" ./cmd/master

setup: ; $(info $(M) Fetching golang dependencies...)
	# Why am I running this twice? see https://github.com/golang/go/issues/27215
	@go get -u github.com/go-bindata/go-bindata
	@go get -u github.com/go-bindata/go-bindata/...

schema: ; $(info $(M) Embedding schema files into binary...)
	@go generate ./api/schema

static: ; $(info $(M) Build static binary for container...)
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
		-a -tags netgo -ldflags '-w -extldflags "-static"' \
		-o target/plantd-master-static cmd/master/*.go

image: static ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY):$(REGISTRY_TAG) -f ./build/Dockerfile.alpine .

container: image ; $(info $(M) Running application container...)
	@docker run -p 3000:3000 $(REGISTRY):$(REGISTRY_TAG)

install: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 target/$(BINARY_NAME) "$(DESTDIR)$(PREFIX)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)$(PREFIX)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)$(PREFIX)/share/licenses/plantd/$(PROJECT)/COPYING"
	@install -Dm 644 configs/$(PROJECT).yaml "$(DESTDIR)$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(DESTDIR)$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)$(PREFIX)/bin/$(BINARY_NAME)

clean: ; $(info $(M) Removing generated files... )
	@rm api/schema/bindata.go
	@rm -rf target/

.PHONY: all build build-deb print-version container coverage coverhtml setup schema static image lint test race msan install uninstall clean
