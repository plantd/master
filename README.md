[![Build Status](https://gitlab.com/plantd/master/badges/master/build.svg)](https://gitlab.com/plantd/master/commits/master)
[![Coverage Report](https://gitlab.com/plantd/master/badges/master/coverage.svg)](https://gitlab.com/plantd/master/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/master)](https://goreportcard.com/report/gitlab.com/plantd/master)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plantd Master

<!-- starter: https://github.com/OscarYuen/go-graphql-starter -->

## Setup

Clone and build the server.

```sh
git clone https://gitlab.com/plantd/master
cd master
make setup
make schema
make
sudo make install
```

Run the database migrations.

```sh
pgcli -h localhost -p 5432 -U postgres
postgres@localhost:postgres> \i data/1.0/1_users.sql
postgres@localhost:postgres> \i data/1.0/2_roles.sql
postgres@localhost:postgres> \i data/1.0/3_rel_users_roles.sql
```

## Publishing

```sh
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
  -a -tags netgo -ldflags '-w -extldflags "-static"' \
  -o bin/plantd-master-static cmd/master/*.go
docker build -t registry.gitlab.com/plantd/master:v1 .
docker push registry.gitlab.com/plantd/master:v1
```

## Running

```sh
docker pull registry.gitlab.com/plantd/master:v1
docker run -it --rm --name=plantd-master \
  -e PLANTD_MASTER_DB_HOST=127.0.0.1 \
  -e PLANTD_MASTER_DB_PORT=5432 \
  -e PLANTD_MASTER_DB_USER=postgres \
  -e PLANTD_MASTER_DB_PASSWORD=postgres \
  -e PLANTD_MASTER_DB_NAME=postgres \
  registry.gitlab.com/plantd/master:v1
```
