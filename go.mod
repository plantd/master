module gitlab.com/plantd/master

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.1.2
	github.com/graph-gophers/dataloader v5.0.0+incompatible
	github.com/graph-gophers/graphql-go v0.0.0-20181128220952-0079757a4d96
	github.com/hashicorp/golang-lru v0.5.1
	github.com/jmoiron/sqlx v0.0.0-20181024163419-82935fac6c1a
	github.com/lib/pq v1.0.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	gitlab.com/plantd/broker v0.2.5-0.20210805033331-bb250b30eace
	gitlab.com/plantd/plantctl v0.1.2
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	google.golang.org/grpc v1.38.0
)

go 1.13
