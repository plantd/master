schema {
    query: Query
    mutation: Mutation
}

# The query type that represents all of the API entrypoints
type Query {
    # Search for a user by email
    user(email: String!): User
    # Retrieve all users with optional paging information
    users(first: Int,  after: String): UsersConnection!

    # Search for a role by name
    role(name: String!): Role
    # Retrieve all roles with optional paging information
    roles(first: Int, after: String): RolesConnection!

    # Search for a configuration by ID
    configuration(id: ID!, namespace: String!): Configuration
    # Retrieve all configurations with optional paging information
    configurations(namespace: String!, first: Int, after: String): ConfigurationsConnection!

    # Search for a module by ID
    module(serviceName: String!, moduleName: String!): Module
    # Retrieve all modules with optional paging information
    modules(first: Int, after: String): ModulesConnection!
    # Retrieve a module's configuration for a given service and module name
    moduleConfiguration(serviceName: String!, moduleName: String!): Configuration
    # Retrieve a module's status for a given service and module name
    moduleStatus(serviceName: String!, moduleName: String!): Status
    # Retrieve a module's settings/properties for a given service and module name
    moduleSettings(serviceName: String!, moduleName: String!): [Property]

    # Search for an experiment by ID
    experiment(id: ID!): Experiment
    # Retrieve all experiments
    experiments(serviceName: String!): Configuration!
    # Get the current experiment
    currentExperiment(): Experiment

    # Search for module job by ID
    moduleJob(serviceName: String!, moduleName: String!, id: ID!): Job
    # Retrive all module jobs
    moduleJobs(serviceName: String!, moduleName: String!, first: Int, after: String): JobConnection!

    # Get a module's property
    moduleProperty(serviceName: String!, moduleName: String! propertyName: String!): Property
    # Get a module's properties
    moduleProperties(serviceName: String!, moduleName: String! propertyNames: [String]): [Property]

    # Get service statuses with Manager
    managerStatus(): [Property]
}

# The Mutation type represents all of the entry points that request modifications
type Mutation {
    # Add a new user to the database
    createUser(email: String!, password: String!): User

    ## TODO: Implement this
    #deleteUser(email: String!): User
    ## TODO: Implement this
    #userChangePassword(email: String!, password: String!): User
    ## TODO: Implement this
    #userAssignRole(email: String!, role: Int!): User
    ## TODO: Implement this
    #userRevokeRole(email: String!, role: Int!): User

    # Add a new role to the database
    createRole(name: String!): Role

    # Send a new configuration to the conf service
    createConfiguration(conf: ConfigurationInput!): Configuration!
    # Update an existing configuration by ID
    updateConfiguration(id: ID!, namespace: String!, conf: ConfigurationInput!): Configuration!
    # Delete an existing configuration by ID
    deleteConfiguration(id: ID!, namespace: String!): Configuration

    # Submit a new job
    submitJob(serviceName: String!, moduleName: String!, action: String!, actionValue: String, properties: [PropertyInput]): Job
    # Cancel a job
    cancelJob(serviceName: String!, moduleName: String!, jobId: ID!): Job
    #pauseJob(serviceName: String!, moduleName: String!, id: ID!): Job
    #resumeJob(serviceName: String!, moduleName: String!, id: ID!): Job
    submitExpUpdateJob(serviceName: String!): Job
    submitExpBundleJob(serviceName: String!, experimentIds: [String]!): Job
    submitExpDeleteJob(serviceName: String!, experimentIds: [String]!): Job

    # Set a module's property
    setModuleProperty(serviceName: String!, moduleName: String!, property: PropertyInput!): Property
    # Set a module's properties
    setModuleProperties(serviceName: String!, moduleName: String!, properties: [PropertyInput]): [Property]

    # Specifically set the recipe properties, which includes id, name, and raw
    setModuleRecipe(serviceName: String!, moduleName: String!, recipeID: String!): [Property]

    # Submit a manager command
    submitManagerCommand(command: String!): Job
}

input ConfigurationInput {
    id: ID
    name: String
    namespace: String!
    objects: [ObjectInput]
    properties: [PropertyInput]
}

input ObjectInput {
    id: ID
    name: String
    objects: [ObjectInput]
    properties: [PropertyInput]
}

input PropertyInput {
    key: String!
    value: String!
}