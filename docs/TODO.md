# ToDo

* [x] Add Docker image from [here][go-ci].
* [ ] Fix GitLab CI
* [ ] Look into doing users and roles in Mongo instead

[go-ci]: https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/
