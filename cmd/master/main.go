package main

import (
	"fmt"
	"net/http"
	"os"
	"regexp"
	"time"

	"gitlab.com/plantd/master/api/schema"
	gcontext "gitlab.com/plantd/master/pkg/context"
	"gitlab.com/plantd/master/pkg/handler"
	"gitlab.com/plantd/master/pkg/loader"
	"gitlab.com/plantd/master/pkg/resolver"
	"gitlab.com/plantd/master/pkg/service"

	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func main() {
	// Set configuration values here
	var (
		addr              = ":3000"
		readHeaderTimeout = 1 * time.Second
		writeTimeout      = 10 * time.Second
		idleTimeout       = 90 * time.Second
		maxHeaderBytes    = http.DefaultMaxHeaderBytes
	)

	if len(os.Args) > 1 {
		r, _ := regexp.Compile("^-v$|^(-{2})?version$")
		if r.Match([]byte(os.Args[1])) {
			fmt.Println(VERSION)
		}
		os.Exit(0)
	}

	// Load configuration file
	config, err := gcontext.LoadConfig(".")
	if err != nil {
		log.Fatalf("Unable to load configuration: %s\n", err)
	}

	// Connect to the database
	db, err := gcontext.OpenDB(config)
	if err != nil {
		log.Fatalf("Unable to connect to db: %s\n", err)
	}

	// Create a configuration service connection
	confClient, err := gcontext.ConfigureClient(config)
	if err != nil {
		log.Fatalf("Unable to connect to configuration service: %s\n", err)
	}

	// Create an Manager service connection
	managerClient, err := gcontext.ManagerClient(config)
	if err != nil {
		log.Fatalf("Unable to connect to manager service: %s\n", err)
	}

	ctx := context.Background()
	logger := service.NewLogger(config)
	roleService := service.NewRoleService(db, logger)
	userService := service.NewUserService(db, roleService, logger)
	authService := service.NewAuthService(config, logger)
	confService := service.NewConfigureService(confClient, logger)
	managerService := service.NewManagerService(managerClient, logger)
	brokerService := service.NewBrokerService(config, logger)

	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "log", logger)
	ctx = context.WithValue(ctx, "roleService", roleService)
	ctx = context.WithValue(ctx, "userService", userService)
	ctx = context.WithValue(ctx, "authService", authService)
	ctx = context.WithValue(ctx, "confService", confService)
	ctx = context.WithValue(ctx, "managerService", managerService)
	ctx = context.WithValue(ctx, "brokerService", brokerService)

	loggerHandler := &handler.LoggerHandler{config.Log.Debug}
	// Create the request handler; inject dependencies
	h := &handler.GraphQL{
		// Parse and validate schema, panic on fail
		Schema:  graphql.MustParseSchema(schema.GetRootSchema(), &resolver.Resolver{}),
		Loaders: loader.NewLoaderCollection(brokerService),
	}

	// Register handlers to routes
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "web/graphiql.html")
	}))
	mux.Handle("/login", handler.AddContext(ctx, allowCors(handler.Login())))
	mux.Handle("/query", handler.AddContext(ctx,
		loggerHandler.Logging(handler.Authenticate(allowCors(h))),
	))

	// Configure HTTP server
	s := &http.Server{
		Addr:              addr,
		Handler:           mux,
		ReadHeaderTimeout: readHeaderTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		MaxHeaderBytes:    maxHeaderBytes,
	}

	log.Printf("Listen for requests on %s", s.Addr)

	if err = s.ListenAndServe(); err != nil {
		log.Println("server.ListenAndServer:", err)
	}

	log.Println("Shut down")
}

func allowCors(handler http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		handler.ServeHTTP(w, r)
	}
}
